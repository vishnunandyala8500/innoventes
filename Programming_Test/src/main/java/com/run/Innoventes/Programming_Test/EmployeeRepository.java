package com.run.Innoventes.Programming_Test;

import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
public interface EmployeeRepository extends JpaRepository<EmployeeDetails, Long>  {
    EmployeeDetails findOne(Long empid);
    
}
