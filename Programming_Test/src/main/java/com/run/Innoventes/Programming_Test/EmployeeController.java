package com.run.Innoventes.Programming_Test;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @Autowired
	EmployeeRepository employeeRepository;
    
	@RequestMapping(method=RequestMethod.GET, value ="/getEmployeeById")
    public EmployeeDetails getEmployeeById(Long empid) {
		System.out.println("Inside getEmployeeById method");
		System.out.println(employeeRepository);
		EmployeeDetails details = employeeRepository.findOne(empid);
		return details;
    	
    }
	@RequestMapping(method=RequestMethod.GET,value="/getAllEmployees")
	public List<EmployeeDetails> getAllEmployees(EmployeeDetails emp){
		
		
		return employeeRepository.findAll();
		
	}
	@RequestMapping(method=RequestMethod.POST,value="/addEmployee")
	public EmployeeDetails createEmployee(@RequestBody EmployeeDetails emp ) {
		
		System.out.println("createEmployee method");
		return employeeRepository.save(emp);
		
	}
	
	@RequestMapping(value = "/updateEmployee", method = RequestMethod.PUT)
    public EmployeeDetails updateEmployee(@RequestBody EmployeeDetails updateEmp)
    {
        return employeeRepository.save(updateEmp);
    }
	@RequestMapping(method=RequestMethod.DELETE,value="/deleteEmployee")
	public void deleteEmployee(Long empid) {
		
		employeeRepository.delete(empid);
		
	}
}
