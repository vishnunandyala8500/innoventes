package com.run.Innoventes.Programming_Test;

import javax.persistence.*;

//import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "EmployeeDetails")
public class EmployeeDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EMPID", unique = true, nullable = false)
	private Long empid;

	public Long getEmpid() {
		return empid;
	}

	public void setEmpid(Long empid) {
		this.empid = empid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private String firstName;
	private String addressType;
	private String lastName;
	private String dOB;

	public String getdOB() {
		return dOB;
	}

	public void setdOB(String dOB) {
		this.dOB = dOB;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	private String designation;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "employeeDetails", cascade = CascadeType.ALL)
	private Addresses address;

	public Addresses getAddresses() {
		return address;
	}

	public void setAddress(Addresses address) {
		this.address = address;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

}
